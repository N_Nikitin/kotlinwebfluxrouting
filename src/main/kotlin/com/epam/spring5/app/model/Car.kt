package com.epam.spring5.app.model

data class Car(val brand : String, val seats : Int, val color: String)