package com.epam.spring5.app.service

class SomeService {

    private val message: String = "I'm not null :)"

    fun getName():String { return message }

}