package com.epam.spring5.app

import com.epam.spring5.app.routing.Routes
import com.epam.spring5.app.routing.handlers.CarHandler
import com.epam.spring5.app.service.SomeService
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans
import org.springframework.web.reactive.function.server.HandlerStrategies
import org.springframework.web.reactive.function.server.RouterFunctions
import org.springframework.web.reactive.result.view.script.ScriptTemplateConfigurer
import org.springframework.web.reactive.result.view.script.ScriptTemplateViewResolver
import org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmLocalScriptEngineFactory

fun beans() = beans {
    bean<CarHandler>()
    bean<Routes>()
    bean<SomeService>("service1")
    bean("webHandler") {
        RouterFunctions.toHttpHandler(
                ref<Routes>().router(), HandlerStrategies.builder().viewResolver(ref()).build()
        )
    }
    bean {
        val prefix = "templates/"
        val suffix = ".kts"
        ScriptTemplateViewResolver().apply {
            setPrefix(prefix)
            setSuffix(suffix)
        }
    }
    bean {
        ScriptTemplateConfigurer().apply {
            engine = KotlinJsr223JvmLocalScriptEngineFactory().scriptEngine
        }
    }
}

class BeansInitializer : ApplicationContextInitializer<GenericApplicationContext> {
    override fun initialize(context: GenericApplicationContext) =
            beans().initialize(context)

}
