package com.epam.spring5.app.routing.handlers

import com.epam.spring5.app.model.Car
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

class CarHandler {

    private val cars = Flux.just(
            Car("BMW", 4, "white"),
            Car("Mercedes", 4, "black"),
            Car("Volvo", 6, "silver"),
            Car("Audi", 2, "black")
    )

    fun getAll(request: ServerRequest) = ok().body(cars)

    fun getByColor(request: ServerRequest) : Mono<ServerResponse> {
        val color: Optional<String> = request.queryParam("color")
        return if (color.isPresent) ok().body(cars.filter { it.color == color.get() })
        else ok().body(Mono.just("Error"))
    }

}