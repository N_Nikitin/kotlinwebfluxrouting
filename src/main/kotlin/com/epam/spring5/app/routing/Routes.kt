package com.epam.spring5.app.routing

import com.epam.spring5.app.routing.handlers.CarHandler
import com.epam.spring5.app.service.SomeService
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono

class Routes(private val carHandler: CarHandler,
             @Qualifier("service1") private val someService1: SomeService,
             @Qualifier("service2") private val someService2: SomeService?
){

    fun router() = router {
        accept(MediaType.TEXT_HTML).nest {

            GET("/") { ok().render("index") }

            "/cars".nest {

                GET("/", carHandler::getAll)
                GET("/color", carHandler::getByColor)

            }

            GET("/hello/{name}") { req ->
                val pathVar = req.pathVariable("name") // queryParam
                ok().body(Mono.just("Hello, $pathVar!"))
            }

            GET("/services") {
                val value = "Service1: ${someService1.getName()}\nService2: ${someService2?.getName()}"
                ok().body(Mono.just(value))
            }

        }
        resources("/**", ClassPathResource("static/"))
    }.filter { serverRequest, handlerFunction ->
        handlerFunction.handle(serverRequest).filter( {!serverRequest.path().equals("/hello/World") })}

}