package com.epam.spring5.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
    System.setProperty("idea.use.native.fs.for.win", "false")
}