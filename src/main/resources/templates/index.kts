import com.epam.spring5.app.model.greet

val suggestion : String = if (Math.random() > 0.5) "come in" else  "go away"

"""<html>
<head>
    <title>Kotlin Routing</title>
</head>
<body>
    <div>
	    <h4>Hello, <span style="color:blue">$greet!<span></h4>
        <h4>Just <span style="color:red">$suggestion</span>!</h4>
	</div>
</body>
</html>"""
